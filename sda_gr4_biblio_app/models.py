from django.db import models


class Room(models.Model):
    room_name = models.CharField(max_length=64, blank=False, unique=True)
    description_room = models.TextField(default="")


class Rack(models.Model):
    rack_name = models.CharField(max_length=64, blank=False, unique=True)
    rack_location = models.TextField(default="")
    rack_description = models.TextField(default="")
    room = Room.room_name
    numbers_of_shelves_in_rack = models.PositiveSmallIntegerField(default=5)
