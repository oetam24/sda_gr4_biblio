from django.apps import AppConfig


class SdaGr4BiblioAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sda_gr4_biblio_app'
